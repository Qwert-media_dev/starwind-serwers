export const SET_NODES = 'SET_NODES'
export const DELETE_NODE = 'DELETE_NODE'

export const nodes = (state = [], {type, payload}) => {
  switch (type) {
    case SET_NODES:
      return payload
    case DELETE_NODE:
      return state.filter(node => node.uid !== payload.uid)
    default:
      return state
  }
}

export const nodesLoaded = (state = false, {type}) => {
  switch (type) {
    case SET_NODES:
      return true
    default:
      return state
  }
}
