import Cookie from 'js-cookie'

const tokenExist = !!Cookie.get('token')

export const SIGN_OUT = 'SIGN_OUT'
export const AUTHENTICATE = 'AUTHENTICATE'

const isAuthenticated = (state = tokenExist, {type}) => {
  switch (type) {
    case AUTHENTICATE:
      return true
    case SIGN_OUT:
      return false
    default:
      return state
  }
}

export default isAuthenticated
