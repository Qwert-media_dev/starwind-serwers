import { combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
import { reducer as modal } from 'redux-modal'
import { reducer as notifs } from 'redux-notifications'
import isAuthenticated from './auth'
import { nodes, nodesLoaded } from './nodes'


const rootReducer = combineReducers({
  nodes,
  nodesLoaded,
  isAuthenticated,
  modal,
  notifs,
  form
})

export default rootReducer
export * from './auth'
export * from './nodes'
