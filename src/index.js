import React from 'react'
import ReactDOM from 'react-dom'
import 'normalize.css'
import './index.css'
import Root from './components/Root'

ReactDOM.render(
  <Root/>,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept('./components/Root', () => {
    const NextApp = require('./components/Root').default
    ReactDOM.render(
      <NextApp/>,
      document.getElementById('root')
    )
  })
}
