import axios from 'axios'
import { toCamel } from '../utils'

const BASE_URL = process.env.NODE_ENV === 'development' ? '//localhost:8100' : ''
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))


/**
 * S2DPlugin
 */

const S2D_PLUGIN_URL = `${BASE_URL}/api/v1/Plugins/S2DPlugin/Commands`

export const login = (login, password) =>
  axios.post(`${S2D_PLUGIN_URL}/Login`, {
    Type: 'DtoLoginData',
    Login: login,
    Password: password
  })
    .then(res => {
      if (!res.data.Response) throw new Error('Authentication failed')
    })

export const getUserName = () => sleep(1000).then(() => 'Administrator')

export const sendForm = (url, values) => sleep(1000).then(() => {
  if (url === '/submit-url') {
    return {message: 'Form successfully submitted'}
  }

  // validation
  if (['john', 'paul', 'george', 'ringo'].includes(values.step0.textarea1)) {
    return {
      step0: {
        textarea1: 'That username is taken'
      }
    }
  }
  return {}
})

export const getNodes = () =>
  axios.post(`${S2D_PLUGIN_URL}/GetNodes`, {})
    .then(
      res => toCamel(res.data.Response),
      console.error // eslint-disable-line
    )

// export const getHostName = (NodeUid) =>
//   axios.post(`${S2D_PLUGIN_URL}/CommandForNode`, {
//     Type: 'DtoCommandForNode',
//     NodeUid,
//     CommandName: 'GetHostName',
//     CommandContext: {Type: 'DtoBase'}
//   })
//     .then(res => res.data.Response);

export const bindNode = (Name, IpAddress) =>
  axios.post(`${S2D_PLUGIN_URL}/BindNode`, {
    Type: 'DtoStorageNode',
    Uid: '',
    Name,
    IpAddress,
    HostName: 'qwerty.local',
    PortNumber: '9500',
    Description: 'Qwerty Description',
    AuthData: {
      Login: '',
      Password: ''
    }
  })
    .then(res => {
      if (!res.data.Response) throw new Error('Something went wrong')
    })

export const unbindNode = Uid =>
  axios.post(`${S2D_PLUGIN_URL}/UnbindNode`, {
    Type: 'DtoStorageNode',
    Uid
  })
    .then(res => {
      if (!res.data.Response) throw new Error('Something went wrong')
    })

/**
 * Get Property
 */

export const getProperty = (pluginUid, PropertyName, NodeUid) =>
  axios.post(`${BASE_URL}/api/v1/Plugins/${pluginUid}/Commands/GetProperty`, {
    Type: 'DtoGetPropertyPluginCommand',
    Uid: '',
    PropertyName,
    NodeUid
  })
    .then(res => toCamel(res.data))

/**
 * Client Controller
 */

export const getViews = uidHost =>
  axios.get(`${BASE_URL}/clientcontroller/Views${uidHost ? '?object=' + uidHost : ''}`)
    .then(res => res.data.Members.map(el => el['@odata.id']))

export const getSingleView = viewUrl =>
  axios.get(`${BASE_URL}${viewUrl}`)
    .then(res => toCamel(res.data))
    .then(({'@odata.id': odataId, '@Copyright': copyright, widgetFrames, widgetInstances, ...rest}) => ({
      ...rest,
      widgetFrames: widgetFrames.map(({'@odata.id': id, widgetInstance, ...rest}) => ({
        ...rest,
        id,
        widgetInstance: widgetInstance['@odata.id']
      })),
      widgetInstances: widgetInstances.map(({'@odata.id': id, parameters, widget, ...rest}) => ({
        ...rest,
        id,
        parameters: toCamel(parameters.reduce(({...rest}, {name, value}) => ({...rest, [name]: value}), {})),
        widget: widget['@odata.id']
      }))
    }))

// export const getWidgets = () =>
//   axios.get(`${BASE_URL}/clientcontroller/Widgets`)
//     .then(res => res.data);
//
// export const getSingleWidget = (widgetUrl) =>
//   axios.get(`${BASE_URL}${widgetUrl}`)
//     .then(res => res.data);
