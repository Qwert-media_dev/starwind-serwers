export const Modal = {
  NODE_DELETE: 'nodeDelete',
  NODE_EDIT: 'nodeEdit',
  NODE_CREATE: 'nodeCreate',
  WIZARD_FORM: 'wizardForm'
}

export const Form = {
  WIZARD_FORM: 'wizardForm'
}

export const Message = {
  NODE_DELETE: 'Node Was Deleted',
  NODE_ADDED: 'Node Was Added Successfully'
}

export const DISMISS_AFTER = 2000 // for notifications
