const isArray = arg => Object.prototype.toString.call(arg) === '[object Array]'
const isObject = arg => Object.prototype.toString.call(arg) === '[object Object]'

const fromPascalToCamel = str => (str.charAt(0).toLowerCase() + str.slice(1) || str).toString()

function transformObjectKeys(initial, transform) {
  if (isArray(initial)) {
    return initial.slice().map(val => transformObjectKeys(val, transform))
  }
  if (isObject(initial)) {
    return Object.keys(initial).reduce((newObj, key) => ({
      ...newObj,
      [transform(key)]: transformObjectKeys(initial[key], transform)
    }), {})
  }
  return initial
}

export function toCamel(data) {
  return transformObjectKeys(data, fromPascalToCamel)
}
