import PropTypes from 'prop-types'
import React from 'react'
import { Field } from 'redux-form'
import CheckBoxField from './fields-form/check-box-field/check-box-field'
import InputField from './fields-form/input-field/input-field'
import RadioButtonField from './fields-form/radio-button-field/radio-button-field'
import SingleSelectField from './fields-form/select-field/select-field'
import SliderField from './fields-form/slider-field/slider-field'
import TwoListsField from './fields-form/two-lists-fields/two-lists-fields'


const FormFieldRender = ({data}) => {
  switch (data.type) {
    case 'textarea':
      return <Field component={InputField} {...data} multiline/>

    case 'input':
      return <Field component={InputField} {...data}/>

    case 'radioButton':
      return <Field component={RadioButtonField} {...data}/>

    case 'checkBox':
      return <Field component={CheckBoxField} {...data}/>

    case 'singleSelect':
      return <Field component={SingleSelectField} {...data}/>

    case 'multiSelect':
      return <Field component={SingleSelectField} {...data} multiple/>

    case 'slider':
      return <Field component={SliderField} {...data}/>

    case 'twoLists':
      return <Field component={TwoListsField} {...data}/>

    default:
      return null
  }
}

FormFieldRender.propTypes = {
  data: PropTypes.object.isRequired
}

export default FormFieldRender
