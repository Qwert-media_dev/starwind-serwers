import React from 'react'
import { DragSource } from 'react-dnd'
import ItemTypes from './item-types'


const ListItem = ({label, connectDragSource, isDragging}) => connectDragSource(
  <div className='two-lists-list-item' style={{opacity: isDragging ? 0.4 : 1}}>{label}</div>
)

export default DragSource(ItemTypes.OPTION, {
  beginDrag: ({label, value}) => ({label, value})
}, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}))(ListItem)
