import PropTypes from 'prop-types'
import React from 'react'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import List from './list'
import './two-lists-fields.css'

class TwoListsField extends React.Component {
  onDrop = (target, input) => option => {
    const value = target === 'list1' ?
      input.value.filter(value => value !== option.value) :
      [...input.value, option.value]
    input.onChange(value)
    input.onBlur()
  }

  render() {
    const {input, meta: {touched, error}, doneMessage, labelOne, labelTwo, data} = this.props
    const selected = [], notSelected = []
    data.forEach(option => (input.value.includes(option.value) ? selected : notSelected).push(option))

    return (
      <div className={
        'two-lists-field ' +
        `two-lists-field_${error ? 'invalid' : 'valid'} ` +
        `two-lists-field_${touched ? 'touched' : 'untouched'}`
      }>
        <div className='two-lists-field__helper-text'>
          {error || doneMessage}
          <i className={`two-lists-field__icon icon ion-ios-${error ? 'close' : 'checkmark'}-outline`}
             aria-hidden='true'/>
        </div>
        <div className='row' style={{margin: '0 -8px'}}>
          <List label={labelOne} data={notSelected} onDrop={this.onDrop('list1', input)}/>
          <List label={labelTwo} data={selected} onDrop={this.onDrop('list2', input)}/>
        </div>
      </div>
    )
  }
}

TwoListsField.propTypes = {
  // from redux form
  input: PropTypes.shape({
    value: PropTypes.array.isRequired
  }).isRequired,
  meta: PropTypes.object.isRequired,
  // from parent
  data: PropTypes.array.isRequired,
  labelOne: PropTypes.string.isRequired,
  labelTwo: PropTypes.string.isRequired,
  doneMessage: PropTypes.string.isRequired
}

export default DragDropContext(HTML5Backend)(TwoListsField)
