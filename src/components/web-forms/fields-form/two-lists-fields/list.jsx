import React from 'react'
import { DropTarget } from 'react-dnd'
import ListItem from './list-item'
import ItemTypes from './item-types'


const List = ({label, data, connectDropTarget, isOver, canDrop}) => {
  const isActive = canDrop && isOver
  let backgroundColor = '#36a9e0'
  if (isActive) {
    backgroundColor = '#3286c4'
  } else if (canDrop) {
    backgroundColor = '#36a9e0'
  }

  return connectDropTarget(
    <div className='col-xs-6'>
      <div className='two-lists-list' style={{backgroundColor}}>
        <div className='two-lists-list__header'>{label}</div>
        <div className='two-lists-list__content'>
          {data.map(item => <ListItem key={item.value} {...item}/>)}
        </div>
      </div>
    </div>
  )
}

export default DropTarget(ItemTypes.OPTION, {
  drop(props, monitor) {
    props.onDrop(monitor.getItem())
  }
}, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop(),
}))(List)
