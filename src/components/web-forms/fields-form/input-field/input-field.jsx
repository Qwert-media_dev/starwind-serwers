import TextField from 'material-ui/TextField'
import PropTypes from 'prop-types'
import React from 'react'
import './input-field.css'


const InputField = ({input, meta: {touched, error, asyncValidating}, label, placeholder, multiline, rows}) =>
  <TextField
    {...input}
    placeholder={placeholder}
    className={
      'text-field ' +
      `text-field_${multiline ? 'multi-line' : 'single-line'} ` +
      `text-field_${!asyncValidating && touched && (error ? 'invalid' : 'valid')}`
    }
    label={[
      label,
      <i className={`icon ion-ios-${error ? 'close' : 'checkmark'}-outline text-field__icon`} aria-hidden='true'
         key={1}/>
    ]}
    helperText={asyncValidating ? 'loading...' : (touched && error) || ' '}
    margin='normal'
    multiline={multiline}
    rows={rows || 4}
  />

InputField.propTypes = {
  // from redux form
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  // from parent
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  multiline: PropTypes.bool,
  rows: PropTypes.number
}

export default InputField
