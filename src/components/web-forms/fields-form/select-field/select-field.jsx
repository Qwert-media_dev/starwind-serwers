import React from 'react'
import './select-field.css'
import PropTypes from 'prop-types'


const getSelectedText = (value, data) => {
  if (value && Array.isArray(value) && value.length) {
    const selected = data.filter(option => value.includes(option.value))
    if (selected.length > 0) return selected.map(({label}) => label).join(', ')
  } else if (value) {
    const selected = data.find(option => option.value === value)
    if (selected) return selected.label
  }
}

const getNewValue = (values, value) => values.includes(value) ?
  values.filter(val => val !== value) :
  [...values, value]

const isSelected = (value, newValue) => Array.isArray(value) ? value.includes(newValue) : value === newValue


const SingleSelectField = ({input, meta: {valid, error, touched, active}, label, data, multiple, placeholder}) => {
  const toggleActive = () => active ? input.onBlur() : input.onFocus()
  const onMoseLeave = () => input.onBlur()
  const onOptionClick = value => () => {
    if (Array.isArray(input.value)) {
      input.onChange(getNewValue(input.value, value))
    } else {
      input.onChange(value)
      input.onBlur()
    }
  }

  return (
    <div className={
      'select-field ' +
      `select-field_${touched ? valid ? 'valid' : 'invalid' : 'untouched'} ` +
      `select-field_${active ? 'active' : 'not-active'} ` +
      `select-field_${multiple ? 'multiple' : 'single'}`
    }>
      <label className='select-field__label'>
        {label}
        <i className={`select-field__icon icon ion-ios-${error ? 'close' : 'checkmark'}-outline`} aria-hidden='true'/>
      </label>
      <div className={'select-field__selected'} onClick={toggleActive}>
        {getSelectedText(input.value, data) || placeholder}
        <i className={`select-field__arrow fa fa-chevron-${active ? 'up' : 'down'}`} aria-hidden='true'/>
      </div>
      <ul className='select-field__options' onMouseLeave={onMoseLeave}>
        {data.map(({value, label}, index) => (
          <li key={index} className={isSelected(input.value, value) ? 'selected' : ''} onClick={onOptionClick(value)}>
            {label}
            <i className='fa fa-check select-field__selected-icon' aria-hidden='true'/>
          </li>
        ))}
      </ul>
      <div className='select-field__error'>{error}</div>
    </div>
  )
}

SingleSelectField.propTypes = {
  // from redux form
  input: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired
  }).isRequired,
  meta: PropTypes.object.isRequired,
  // from parent
  label: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  multiple: PropTypes.bool,
  placeholder: PropTypes.string
}

export default SingleSelectField
