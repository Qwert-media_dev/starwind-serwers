import PropTypes from 'prop-types'
import React from 'react'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
import './slider-field.css'


const SliderField = ({input, meta: {error, touched, active}, label, minValue, maxValue, step, minText, maxText}) =>
  <div className={
    'slider-field ' +
    `slider-field_${touched ? 'touched' : 'untouched'} ` +
    `slider-field_${!error ? 'valid' : 'invalid'} ` +
    `slider-field_${active ? 'active' : 'not-active'}`
  }>
    <div className='slider-field__label'>
      {label}
      <i className={`slider-field__icon icon ion-ios-${error ? 'close' : 'checkmark'}-outline`} aria-hidden='true'/>
    </div>
    <Slider
      min={minValue}
      max={maxValue}
      step={step}
      value={input.value}
      onChange={input.onChange}
      onChangeStart={() => input.onFocus()}
      onChangeComplete={() => input.onBlur()}
      labels={{
        [minValue]: minText,
        [maxValue]: maxText
      }}
    />
    <div className='slider-field__error'>{error || 'Success'}</div>
  </div>

SliderField.propTypes = {
  // from redux form
  input: PropTypes.shape({
    value: PropTypes.number.isRequired
  }).isRequired,
  meta: PropTypes.object.isRequired,
  // form parent
  minValue: PropTypes.number.isRequired,
  maxValue: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  minText: PropTypes.string.isRequired,
  maxText: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
}

export default SliderField
