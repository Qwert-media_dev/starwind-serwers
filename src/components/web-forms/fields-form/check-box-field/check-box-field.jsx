import PropTypes from 'prop-types'
import React from 'react'
import './check-box-field.css'

const onChange = (newValue, {value, onChange, onBlur}) => () => {
  onChange(value.includes(newValue) ? value.filter(val => val !== newValue) : [...value, newValue])
  onBlur()
}

const CheckBoxField = ({input, meta: {valid, error, touched}, data}) => {
  return (
    <div className={
      'checkbox-group ' +
      `checkbox-group_${touched ? valid > 0 ? 'valid' : 'invalid' : 'untouched'}`
    }>
      {data.map(({value, label}, index) =>
        <div key={index} className='checkbox-group__item'>
          <input type='checkbox' name={input.name} value={value} checked={input.value.includes(value)}
                 onChange={() => null}/>
          <span className='checkbox-group__indicator' onClick={onChange(value, input)}/>
          <label onClick={onChange(value, input)}>{label}</label>
          <i className='icon ion-ios-checkmark-outline checkbox-group__icon' aria-hidden='true'/>
        </div>
      )}
      <div className='checkbox-group__error'>{error || 'Success'}</div>
    </div>
  )
}

CheckBoxField.propTypes = {
  // from redux form
  input: PropTypes.shape({
    value: PropTypes.array.isRequired
  }).isRequired,
  meta: PropTypes.object.isRequired,
  // from parent
  data: PropTypes.array.isRequired
}

export default CheckBoxField
