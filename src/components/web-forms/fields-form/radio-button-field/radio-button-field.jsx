import PropTypes from 'prop-types'
import React from 'react'
import './radio-button-field.css'


const RadioButtonField = ({input, meta: {error, valid, touched}, data}) =>
  <div className={
    'radio-group ' +
    `radio-group_${touched ? valid ? 'valid' : 'invalid' : 'untouched'}`
  }>
    {data.map(({value, label}, index) =>
      <div key={index} className='radio-group__item'>
        <input type='radio' name={input.name} value={value} checked={input.value === value} onChange={() => null}/>
        <label onClick={() => {input.onChange(value); input.onBlur()}}>
          {label}
          <i className='icon ion-ios-checkmark-outline radio-group__icon' aria-hidden='true'/>
        </label>
      </div>
    )}
    <div className='radio-group__error'>{error}</div>
  </div>


RadioButtonField.propTypes = {
  // from redux form
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  // from parent
  data: PropTypes.array.isRequired
}

export default RadioButtonField
