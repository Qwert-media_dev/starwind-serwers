import React from 'react'
import { connect } from 'react-redux'
import { FormSection, getFormSyncErrors, reduxForm } from 'redux-form'
import { Form as FormNames } from '../../constants'
import './FormContent.css'
import FormFieldRender from './FormField'


let FormContent = ({data: {steps}, handleSubmit, pristine, reset, submitting, formSyncErrors}) =>
  <form className='form-render' onSubmit={handleSubmit}>
    {steps.map((step, stepIndex) => {
      const stepValid = !formSyncErrors || !formSyncErrors[`step${stepIndex}`]
      const icon = stepValid ? 'checkmark' : 'circle'
      const isLastStep = stepIndex + 1 === steps.length

      return <div key={stepIndex} className={
        'form-render__step row ' +
        `form-render__step_${stepValid ? 'valid' : 'invalid'}`
      }>
        <div className='form-render__step-left col-xs-1'>
          <i className={`form-render__step-icon icon ion-ios-${icon}-outline`}/>
          {!isLastStep && <i className='form-render__step-line'/>}
        </div>

        <div className='col-xs-11'>
          <div className='form-render__header'>
            <div className='form-render__step-number'>{`Step ${stepIndex + 1} / ${steps.length}`}</div>
            <div className='form-render__step-title'>{step.name}</div>
          </div>

          <FormSection name={`step${stepIndex}`}>
            {step.rows.map((row, rowIndex) =>
              <div key={rowIndex} className='row'>
                {row.cols.map(({width, element}, colIndex) =>
                  <div key={colIndex} className={'col-xs-' + width}>
                    {
                      element.type === 'submitButton' ?
                        <button className='btn btn-primary' type='submit' disabled={pristine || submitting}>
                          {element.label}
                        </button> :

                        element.type === 'resetButton' ?
                          <button className='btn btn-secondary' type='button' disabled={pristine || submitting}
                                  onClick={reset}>
                            {element.label}
                          </button> :

                          <FormFieldRender data={element}/>
                    }
                  </div>
                )}
              </div>
            )}
          </FormSection>
        </div>
      </div>
    })}
  </form>

FormContent.propTypes = {}

export default connect(state => ({
  formSyncErrors: getFormSyncErrors(FormNames.WIZARD_FORM)(state)
}))(reduxForm({
  form: FormNames.WIZARD_FORM
})(FormContent))
