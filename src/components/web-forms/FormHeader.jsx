import React from 'react'
import { connect } from 'react-redux'
import { getFormSyncErrors } from 'redux-form'
import { Form } from '../../constants'
import './FormHeader.css'


class FormHeader extends React.Component {
  state = {
    stepsListVisible: false
  }

  toggleStepsListVisibility = () => this.setState({stepsListVisible: !this.state.stepsListVisible})

  render() {
    const {data, handleHide, formSyncErrors} = this.props
    const {stepsListVisible} = this.state

    if (data) return (
      <header className='main-header form-header'>
        <div className={
          'form-header__steps ' +
          `form-header__steps_${stepsListVisible ? 'active' : 'not-active'}`
        }>
          <div className='form-header__steps-header' onClick={this.toggleStepsListVisibility}>
            Steps <i className={`icon ion-${stepsListVisible ? 'ios-close-empty' : 'navicon-round'}`}/>
          </div>

          <div className='form-header__steps-list'>
            {data.steps.map((step, index) => {
              const stepValid = !formSyncErrors || !formSyncErrors[`step${index}`]
              const isLastStep = index + 1 === data.steps.length

              return <div key={index} className={
                'form-header__step row ' +
                `form-header__step_${stepValid ? 'valid' : 'invalid'}`
              }>
                <div className='form-header__step-left col-xs-3'>
                  <i className={`form-header__step-icon icon ion-ios-${stepValid ? 'checkmark' : 'circle'}-outline`}/>
                  {!isLastStep && <i className='form-header__step-line'/>}
                </div>
                <div className='form-header__step-right col-xs-9'>
                  <div className='form-header__step-number'>{`Step ${index + 1}`}</div>
                  <div className='form-header__step-title'>{step.name}</div>
                </div>
              </div>
            })}
          </div>
        </div>

        <button className='form-header__close' onClick={handleHide}>
          Close <i className='icon ion-ios-close-empty'/>
        </button>
      </header>
    )
  }
}

FormHeader.propTypes = {}

export default connect(state => ({
  formSyncErrors: getFormSyncErrors(Form.WIZARD_FORM)(state)
}))(FormHeader)
