import Slide from 'material-ui/transitions/Slide'
import React from 'react'
import { connectModal } from 'redux-modal'
import { Modal } from '../../constants'
import FormContent from './FormContent'
import FormHeader from './FormHeader'
import './WIzardForm.css'

const WizardForm = ({show, data, handleHide, handleDestroy, initialValues, validate, onSubmit, onSubmitFail, asyncValidate}) =>
  <Slide in={show} direction='left' onExited={() => handleDestroy()}>
    <div className='wizard-form'>
      <FormHeader data={data} handleHide={handleHide}/>
      <FormContent data={data}
                   initialValues={initialValues}
                   validate={validate}
                   asyncValidate={asyncValidate}
                   asyncBlurFields={data.asyncBlurFields}
                   onSubmit={onSubmit}
                   onSubmitFail={onSubmitFail}
      />
    </div>
  </Slide>

export default connectModal({
  name: Modal.WIZARD_FORM,
  destroyOnHide: false
})(WizardForm)
