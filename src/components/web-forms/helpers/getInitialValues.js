export default function getInitialValues(data) {
  const initialValues = {}

  data.steps.forEach((step, index) => step.rows.forEach(row => row.cols.forEach(({element: {defaultValue, type, name}}) => {
    if (!defaultValue) {
      defaultValue = ['checkBox', 'multiSelect', 'twoLists'].includes(type) ? [] :
        type === 'slider' ? 0 :
          null
    }

    if (defaultValue !== null) {
      const step = `step${index}`
      if (!initialValues[step]) {
        initialValues[step] = {}
      }
      initialValues[step][name] = defaultValue
    }
  })))

  return initialValues
}
