import Rules from './validationRules'

export default function getValidate(data) {
  return function validate(values) {
    const errors = {}

    data.steps.forEach((step, stepIndex) => step.rows.forEach(row => row.cols.forEach(({element: {name, rules}}) => {
      if (!rules) return

      const step = `step${stepIndex}`
      const value = values[step] ? values[step][name] : null

      for (let rule in rules) {
        const param = rules[rule]

        if (rule in Rules) {
          const err = Rules[rule](value, param)

          if (err) {
            if (!errors[step]) errors[step] = {}
            errors[step][name] = err
            return
          }
        }
      }
    })))

    return errors
  }
}
