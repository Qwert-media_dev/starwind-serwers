const validationRules = {
  required: value => (value ? undefined : 'Required'),
  number: value => value && isNaN(Number(value)) ? 'Must be a number' : undefined,
  email: value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined,
  alphaNumeric: value => value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined,
  maxLength: (value, max) => value && value.length > max ? `Must be ${max} characters or less` : undefined,
  minLength: (value, min) => value && value.length < min ? `Must be ${min} characters or more` : undefined,
  maxValue: (value, max) => value && value > max ? `Must be not greater than ${max}` : undefined,
  minValue: (value, min) => value < min ? `Must be at least ${min}` : undefined,
  custom: (value, {pattern, error}) => value && new RegExp(pattern, 'i').test(value) ? undefined : error
}

export default validationRules
