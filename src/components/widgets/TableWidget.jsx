import React, {Component} from 'react'
import './TableWIdget.css'
import withData from './withData'

class TableWidget extends Component {
  static defaultProps = {
    width: 900,
    title: '',
    tableData: [],
    tableDescription: [],
    initialLoaded: false,
    error: false
  };

  render() {
    const {title, tableData, tableDescription, initialLoaded, error} = this.props
    const {width} = this.props

    if (error)
      return <div className="widget text-center" style={{width}}>Sorry, something went wrong.</div>

    if (!initialLoaded)
      return <div className="widget text-center" style={{width}}>Loading...</div>

    return (
      <div className="widget table-widget">
        <h3 className="widget-title table-widget-title">{title}</h3>

        <table style={{width}}>
          <thead>
            <tr>
              {tableDescription.map((headerCell, index) => <th key={index}>{headerCell.title}</th>)}
            </tr>
          </thead>

          <tbody>
            {tableData.map((tableRow, rowIndex) => (
              <tr key={rowIndex}>
                {tableRow.map((dataCell, index) => (
                  <td key={index}>
                    {tableDescription[index].type === 'progress_bar' ?
                      <div style={{
                        height: 16,
                        background: `linear-gradient(to right, #1189ca ${dataCell}%, #f0f0f0 ${dataCell}%)`
                      }} />
                      :
                      dataCell
                    }
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default withData(TableWidget)
