const formatWidgetProperty = ({percent, value, valueUnits, maxValue, maxValueUnits, ...rest}) => {
  percent = +percent
  if (!value && percent > 0) {
    value = percent
    valueUnits = '%'
  }
  if (!maxValue && valueUnits === '%') maxValue = 100
  if (!maxValueUnits && valueUnits) maxValueUnits = valueUnits
  if (percent < 0 && value && maxValue) percent = Math.round(+value / +maxValue * 100)
  return {percent, value, valueUnits, maxValue, maxValueUnits, ...rest}
}

export default formatWidgetProperty
