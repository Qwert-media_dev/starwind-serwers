import React, { Component } from 'react'
import formatWidgetProperty from './helpers/formatWidgetProprty'
import './ProgressBarWidget.css'
import withData from './withData'


class ProgressBarWidget extends Component {
  static defaultProps = {
    width: 293,
    height: 258,
    title: '',
    value: 0,
    valueUnits: '',
    maxValue: 0,
    maxValueUnits: '',
    percent: null,
    initialLoaded: false,
    error: false
  }

  render() {
    const {width, height, title, value, valueUnits, maxValue, maxValueUnits, percent, error, initialLoaded} = formatWidgetProperty(this.props)
    const background = `linear-gradient(to right, #1189ca ${percent}%, #f0f0f0 ${percent}%)`

    if (error)
      return <div className="widget text-center" style={{width, height}}>Sorry, something went wrong.</div>

    if (!initialLoaded)
      return <div className="widget text-center" style={{width, height}}>Loading...</div>

    return (
      <div className="widget progress-bar-widget" style={{width, height}}>
        <h3 className="widget-title progress-bar-widget-title">{title}</h3>
        <div className="progress-bar-widget-value">
          <span style={{fontSize: 48}}>{value}</span> <span style={{fontSize: 28}}>{valueUnits}</span>
        </div>
        <div className="progress-bar-widget-text">
          <span>{title}</span>
          <span>{value}
            <small>{valueUnits}</small> / {maxValue}
            <small>{maxValueUnits}</small></span>
        </div>
        <div className="progress-bar-widget-progress" style={{background}}/>
      </div>
    )
  }
}

export default withData(ProgressBarWidget)
