import React, { Component } from 'react'
// import Knob from 'react-canvas-knob'
import Knob from '../../libs/Knob'
import './ArcWidget.css'
import formatWidgetProperty from './helpers/formatWidgetProprty'
import withData from './withData'


class ArcWidget extends Component {
  static defaultProps = {
    width: 293,
    height: 258,
    title: '',
    value: 0,
    valueUnits: '',
    maxValue: 0,
    maxValueUnits: '',
    percent: null,
    initialLoaded: false,
    error: false
  };

  render() {
    const { width, height, title, value, valueUnits, maxValue, maxValueUnits, percent, initialLoaded, error} = formatWidgetProperty(this.props)
    const showMaxValue = maxValue && valueUnits !== '%'
    const showKnob = percent > 0
    const smallText = showKnob && value.toString().length + valueUnits.length > 5

    if (error)
      return <div className="widget text-center" style={{width, height}}>Sorry, something went wrong.</div>

    if (!initialLoaded)
      return <div className="widget text-center" style={{width, height}}>Loading...</div>

    return (
      <div className="widget arc-widget" style={{width, height}}>
        <h3 className="widget-title arc-widget-title">{title}</h3>

        <div className="arc-widget-chart-wrap">
          <Knob
            value={showKnob ? percent : 0}
            onChange={() => null}
            bgColor={showKnob ? '#f0f0f0' : 'transparent'}
            fgColor='#1189ca'
            angleOffset={-125}
            angleArc={250}
            thickness={0.20}
            readOnly
            width={180}
            height={170}
            displayInput={false}
          />
          <div className="arc-widget-text">
            <span style={{fontSize: smallText ? 48 * 0.75 : 48}}>{value}</span>&nbsp;
            <span style={{fontSize: smallText ? 28 * 0.75 : 28}}>{valueUnits}</span>
            {showMaxValue && <div>FROM {maxValue} {maxValueUnits}</div>}
          </div>
        </div>
      </div>
    )
  }
}

export default withData(ArcWidget)
