import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { showForm } from '../../actions'
import './InfoTileWidget.css'
import withData from './withData'


const Button = ({width, height, color, backgroundColor, title, onClick}) =>
  <button style={{width, height, color, backgroundColor}}
          className='info-box-widget-raisedButtons'
          onClick={onClick}
  >{title}</button>


const Message = ({style, children}) => <div className='widget text-center' style={style}>{children}</div>


const InfoTileWidget = ({width, height, parameters, nodeUid, initialLoaded, error, title, text, img, showForm}) => {
  const style = {width, height}
  const {viewWizardButton, dataSource: {pluginUid}} = parameters
  const dataSource = viewWizardButton ? viewWizardButton.actions.dataSource : null

  if (error)
    return <Message style={style}>Sorry, something went wrong.</Message>

  if (!initialLoaded)
    return <Message style={style}>Loading...</Message>

  return (
    <div className='widget info-box-widget' style={style}>
      {img && <img src={img} alt='' className='info-box-widget-img'/>}
      <h3 className='widget-title info-box-widget-title' dangerouslySetInnerHTML={{__html: title}}/>
      <p className='info-box-widget-text' dangerouslySetInnerHTML={{__html: text}}/>
      {viewWizardButton && <Button {...viewWizardButton} onClick={() => showForm(pluginUid, dataSource, nodeUid)}/>}
    </div>
  )
}

InfoTileWidget.defaultProps = {
  width: 293,
  height: 258,
  title: '',
  text: '',
  img: null,
}

InfoTileWidget.propTypes = {
  // from parent
  width: PropTypes.number,
  height: PropTypes.number,
  parameters: PropTypes.object.isRequired,
  nodeUid: PropTypes.string.isRequired,
  // from withData decorator
  initialLoaded: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  title: PropTypes.string,
  text: PropTypes.string,
  img: PropTypes.string,
  // from connect
  showForm: PropTypes.func
}

export default withData(connect(null, {showForm})(InfoTileWidget))
