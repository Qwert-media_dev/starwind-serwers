import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { getProperty } from '../../dataProviders'


export default function withData(WrappedComponent) {
  return class withDataDecorator extends Component {
    static propTypes = {
      width: PropTypes.number,
      height: PropTypes.number,
      parameters: PropTypes.object.isRequired,
      nodeUid: PropTypes.string.isRequired
    }

    state = {
      initialLoaded: false,
      error: false
    }

    componentDidMount() {
      this.getData()
    }

    componentWillUnmount() {
      clearTimeout(this.timeoutID)
      this.handleSuccess = () => null
      this.handleError = () => null
    }

    getData = ({parameters, nodeUid} = this.props) => {
      const {pluginUid, propertyName} = parameters.dataSource
      getProperty(pluginUid, propertyName, nodeUid)
        .then(data => this.handleSuccess(data), err => this.handleError(err))
    }

    handleSuccess = data => {
      this.setState({...data, initialLoaded: true})
      const {refreshIntervalMs} = this.props.parameters
      const delay = refreshIntervalMs ?
        Math.max(refreshIntervalMs, window.MIN_REFRESH_INTERVAL) :
        window.DEFAULT_REFRESH_INTERVAL
      this.timeoutID = setTimeout(this.getData, delay)
    }

    handleError = (err) => {
      console.error(err);
      this.setState({error: true})
    }

    render() {
      return <WrappedComponent {...this.state} {...this.props}/>
    }
  }
}
