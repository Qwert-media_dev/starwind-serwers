import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'


const NodeRoute = ({component: Component, nodes, nodesLoaded, ...rest}) => (
  <Route {...rest} render={props => (
    nodesLoaded && !nodes.find(node => node.uid === props.match.params.uid) ?
      <Redirect to={{
        pathname: '/',
        state: {from: props.location}
      }}/>
      :
      <Component {...props} key={props.match.params.uid} />
  )}/>
)

const mapStateToProps = state => ({
  nodes: state.nodes,
  nodesLoaded: state.nodesLoaded
})

export default connect(mapStateToProps, {})(NodeRoute)
