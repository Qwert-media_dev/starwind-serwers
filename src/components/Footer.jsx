import React from 'react'
import './Footer.css'


const Footer = () => {
  return (
    <footer className="main-footer row">
      <div className="col-xs">Copyright © 2017 • Starwind Web Interface</div>
      <div className="col-xs text-right">Developed by <a href="" className="link">QWERT-Media</a></div>
    </footer>
  )
}

export default Footer
