import TextField from 'material-ui/TextField'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { authenticate } from '../actions'
import logo from '../img/starwind-logo.png'
import Footer from './Footer'
import './Login.css'


class Login extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    authenticate: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired
  }

  state = {
    userName: '',
    password: '',
    remember: false,
    error: false,
    showPassword: false
  }

  handleChange = e => {
    this.setState({[e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value})
  }

  handleSubmit = e => {
    e.preventDefault()
    const {userName, password, remember} = this.state
    this.props.authenticate(userName, password, remember).catch(() => this.setState({error: true}))
  }

  togglePasswordVisibility = () => {
    this.setState({showPassword: !this.state.showPassword})
  }

  render() {
    const {from} = this.props.location.state || {from: {pathname: '/'}}
    const redirectToReferrer = this.props.isAuthenticated

    if (redirectToReferrer)
      return <Redirect to={from}/>

    return (
      <div className='login-page'>
        <div className='login-page-content'>
          <div className='login-box'>
            <div className='login-logo'>
              <img src={logo} alt='StarWind' width='128.5' height='14'/>
            </div>
            {/* /.login-logo */}

            <div className='login-box-body'>
              <h2 className='login-box-title'>Login</h2>
              <p className='login-box-msg'>Log In and start managing <br/> your system</p>
              <form onSubmit={this.handleSubmit}>
                <TextField
                  label='User Name'
                  value={this.state.userName}
                  onChange={e => this.setState({userName: e.target.value})}
                  error={this.state.error}
                  style={{marginBottom: 17}}
                  fullWidth
                />
                <div className='text-field-wrap'>
                  <TextField
                    label='Password'
                    value={this.state.password}
                    onChange={e => this.setState({password: e.target.value})}
                    error={this.state.error}
                    type={this.state.showPassword ? 'text' : 'password'}
                    style={{marginBottom: 17}}
                    fullWidth
                  />
                  <button type='button' className='show-password-btn' onClick={this.togglePasswordVisibility}>
                    <i className='fa fa-eye'/></button>
                </div>
                <div className='row' style={{marginBottom: 35}}>
                  <div className='col-xs'>
                    <label className='custom-control custom-checkbox'>
                      <input type='checkbox' className='custom-control-input' name='remember'
                        checked={this.state.remember} onChange={this.handleChange}/>
                      <span className='custom-control-indicator'/>
                      <span className='custom-control-description'>Keep me signed in</span>
                    </label>
                  </div>
                  <div className='col-xs text-right'>
                    {/*<a href='#' className='link login-box-restore-password-link'>Forgot your password?</a>*/}
                  </div>
                </div>
                <button type='submit' className='btn btn-primary align-center'>Enter</button>
              </form>
            </div>
            {/* /.login-box-body */}
          </div>
          {/* /.login-box */}
        </div>
        <Footer/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.isAuthenticated
})

export default connect(mapStateToProps, {authenticate})(Login)
