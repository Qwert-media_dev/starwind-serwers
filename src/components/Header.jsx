import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { signOut } from '../actions'
import { getUserName } from '../dataProviders'
import userAvatar from '../img/user-avatar.png'
import './Header.css'


class Header extends React.Component {
  state = {
    userName: '...',
  }

  componentDidMount() {
    getUserName().then(userName => this.setState({userName}))
  }

  render() {
    const {signOut} = this.props

    return (
      <header className="main-header">
        <div className="header-user">
          <img src={userAvatar} width={16} height={20} alt="avatar"/>
          {this.state.userName}
        </div>
        <button className="btn header-sign-out-btn" onClick={signOut}/>
      </header>
    )
  }
}

Header.propTypes = {
  signOut: PropTypes.func.isRequired,
}

export default connect(null, {signOut})(Header)
