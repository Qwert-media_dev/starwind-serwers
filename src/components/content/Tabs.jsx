import React from 'react'
import PropTypes from 'prop-types'
import ListItemLink from '../navigation/ListItemLink'
import './Tabs.css'


const Tabs = ({ views, uid }) => (
  <ul  className="nav-tabs">
    {views.map((view, index) =>
      <ListItemLink key={index} to={`/nodes/${uid}/views/${view.id}`}>{view.name}</ListItemLink>
    )}
  </ul>
)

Tabs.propTypes = {
  views: PropTypes.array.isRequired,
  uid: PropTypes.string.isRequired
}

export default Tabs
