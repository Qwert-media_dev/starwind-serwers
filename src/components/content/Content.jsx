import React from 'react'
import { Redirect } from 'react-router-dom'
import { getSingleView, getViews } from '../../dataProviders/index'
import './Content.css'
import Tabs from './Tabs'
import TabView from './TabView'

const message = text => <div style={{padding: 24, textAlign: 'center'}}>{text}</div>
let prevView


class Content extends React.Component {
  state = {
    views: [],
    loaded: false,
    error: false
  }

  componentWillMount() {
    this.getData()
  }

  componentWillUnmount() {
    prevView = this.props.match.params.view
  }

  getData({match} = this.props) {
    getViews(match.params.uid).then(views => {
      Promise.all(views.map(view => getSingleView(view)))
        .then(this.handleSuccess, this.handleError)
    }, this.handleError)
  }

  handleSuccess = views => {
    this.setState({
      views,
      loaded: true,
      error: false
    })
  }

  handleError = err => {
    console.error(err) // eslint-disable-line
    this.setState({error: true})
  }

  render() {
    const {views, loaded, error} = this.state
    const {match} = this.props

    if (error) return message('Sorry, something went wrong.')

    if (!loaded) return message('Loading...')

    if (views.length && !match.params.view) {
      const view = views.find(view => view.id === prevView) ? prevView : views[0].id
      return <Redirect to={`/nodes/${match.params.uid}/views/${view}`}/>
    }

    return (
      <div className='content'>
        <Tabs uid={match.params.uid} views={views}/>
        <TabView uid={match.params.uid} view={views.find(view => view.id === match.params.view)}/>
      </div>
    )
  }
}

export default Content
