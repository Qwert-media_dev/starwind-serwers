import React from 'react'
import PropTypes from 'prop-types'
import ProgressBarArc from '../widgets/ArcWidget'
import ProgressBarWidget from '../widgets/ProgressBarWidget'
import InfoTileWidget from '../widgets/InfoTileWidget'
import TableWidget from '../widgets/TableWidget'

const widgets = {ProgressBarArc, ProgressBarWidget, InfoTileWidget, TableWidget}


const TabView = ({uid, view}) => {
  const matrix = []
  const flexWrap = view.widgetFloatingOnResize ? 'wrap' : 'nowrap'

  view.widgetFrames.forEach(({horizontalOffset, horizontalOrder, verticalOffset, verticalOrder, widgetInstance}) => {
    const ver = verticalOffset + verticalOrder - 1
    const hor = horizontalOffset + horizontalOrder - 1
    const widget = view.widgetInstances.find(widget => widget.id === widgetInstance)

    if (!matrix[ver]) matrix[ver] = []
    matrix[ver][hor] = widget
  })

  return (
    <div style={{ padding: '12px 0', zIndex: 1 }}>
      {matrix.map((row, i) =>
        <div key={i} style={{ display: 'flex', flexWrap }}>
          {row.map(({widget, width, height, parameters, memberId}) => {
            const Widget = widgets[widget.split('/').pop()]
            return <Widget key={memberId} width={width} height={height} parameters={parameters} nodeUid={uid} />
          })}
        </div>
      )}
    </div>
  )
}

TabView.propTypes = {
  uid: PropTypes.string.isRequired,
  view: PropTypes.object.isRequired
}

export default TabView
