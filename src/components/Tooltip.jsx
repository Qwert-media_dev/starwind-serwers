import React from 'react'
import PropTypes from 'prop-types'
import Tippy from 'tippy.js'
import 'tippy.js/dist/tippy.css'
import './Tooltip.css'


const Tooltip = ({children, title}) => {
  let onMouseEnter = e => {
    if (e.target.offsetWidth < e.target.scrollWidth && !e.target.hasAttribute('title') && !e.target.hasAttribute('data-original-title')) {
      e.target.setAttribute('title', title)
      new Tippy(e.target, {
        theme: 'light',
        arrow: true
      })
    }
  }

  return React.cloneElement(children, {onMouseEnter})
}

Tooltip.propTypes = {
  children: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired
}

export default Tooltip
