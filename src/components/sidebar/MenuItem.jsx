import Collapse from 'material-ui/transitions/Collapse'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { show as showModal } from 'redux-modal'
import { getNodes } from '../../actions/index'
import { Modal } from '../../constants'
import NodeCreateDialog from '../dialogs/NodeCreateDialog'
import Tooltip from '../Tooltip'
import SubMenuItem from './SubMenuItem'

const expanded = true
const CLUSTER_IS_LINK = false


class SidebarMenuItem extends Component {
  static propTypes = {
    // form connect
    nodes: PropTypes.array.isRequired,
    getNodes: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    // form react-router
    location: PropTypes.object.isRequired,
  }

  state = {
    expanded,
    collapseEntered: expanded
  }

  componentDidMount() {
    this.props.getNodes()
  }

  handleAddNewClick = () => {
    this.props.showModal(Modal.NODE_CREATE)
  }

  toggleExpanded = e => {
    e.preventDefault()
    this.setState({expanded: !this.state.expanded})
  }

  render() {
    const {expanded, collapseEntered} = this.state
    const {nodes, location} = this.props

    return (
      <li className={'with-sub-menu' + (expanded ? ' expanded' : '')}>
        <Tooltip title='Cluster SW-DEVELOPER'>
          {CLUSTER_IS_LINK ?
            <NavLink to="/clusters/123">Cluster SW-DEVELOPER</NavLink> :
            <a href="" onClick={this.toggleExpanded}>Cluster SW-DEVELOPER</a>
          }
        </Tooltip>
        <button className="btn toggle-sub-menu-btn" onClick={this.toggleExpanded}><i className="fa fa-angle-down"/>
        </button>

        <Collapse in={expanded}
                  transitionDuration="auto"
                  unmountOnExit
                  onEntered={() => this.setState({collapseEntered: true})}
                  onExit={() => this.setState({collapseEntered: false})}
                  style={{overflow: collapseEntered ? 'visible' : 'hidden'}}
        >
          <ul className="sidebar-sub-menu">
            {nodes.map((host, index) => <SubMenuItem key={index} host={host} location={location}/>)}
          </ul>
          <button className="btn sidebar-add-new-btn" onClick={this.handleAddNewClick}>
            + Add New Node
          </button>
          <NodeCreateDialog/>
        </Collapse>
      </li>
    )
  }
}

const mapStateToProps = state => ({
  nodes: state.nodes
})

export default connect(mapStateToProps, {getNodes, showModal})(SidebarMenuItem)
