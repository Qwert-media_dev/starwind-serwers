import React from 'react'
import logo from '../../img/starwind-logo-white.png'
import './MainSidebar.css'
import SidebarMenuItem from './MenuItem'


class MainSidebar extends React.Component {
  render() {
    return (
      <aside className="main-sidebar">
        <div className="sidebar-logo">
          <img src={logo} alt="StarWind" width={170} height={18.5}/>
        </div>

        <ul className="sidebar-menu">
          <li className="header">MENU</li>
          <SidebarMenuItem location={this.props.location}/>
        </ul>
      </aside>
    )
  }
}

MainSidebar.propTypes = {}

export default MainSidebar
