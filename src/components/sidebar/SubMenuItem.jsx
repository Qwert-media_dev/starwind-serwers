import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { show as showModal } from 'redux-modal'
import { Modal } from '../../constants'
import Tooltip from '../Tooltip'

class SidebarSubMenuItem extends Component {
  static propTypes = {
    // from parent
    host: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    // from connect
    showModal: PropTypes.func.isRequired
  }

  state = {
    isNodeActionsVisible: false
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocumentClick)
    document.removeEventListener('keydown', this.handleDocumentKeydown)
  }

  toggleNodeActionsVisibility = () => {
    this.setState({isNodeActionsVisible: !this.state.isNodeActionsVisible}, () => {
      const method = this.state.isNodeActionsVisible ? 'addEventListener' : 'removeEventListener'
      document[method]('click', this.handleDocumentClick)
      document[method]('keydown', this.handleDocumentKeydown)
    })
  }

  handleDocumentClick = e => {
    if (!e.target.closest('.sidebar-node-actions')) this.toggleNodeActionsVisibility()
  }

  handleDocumentKeydown = e => {
    if (e.keyCode === 27) this.toggleNodeActionsVisibility()
  }

  handleEditClick = node => () => {
    this.toggleNodeActionsVisibility()
    this.props.showModal(Modal.NODE_EDIT, {node})
  }

  handleDeleteClick = node => () => {
    this.toggleNodeActionsVisibility()
    this.props.showModal(Modal.NODE_DELETE, {node})
  }

  render() {
    const {isNodeActionsVisible} = this.state
    const {host} = this.props

    // TODO: change ipAddresss to name

    return (
      <li key={host.Uid} className={isNodeActionsVisible ? 'node-actions-visible' : ''}>
        <Tooltip title={host.ipAddress}>
          <NavLink to={`/nodes/${host.uid}`}>{host.ipAddress}</NavLink>
        </Tooltip>
        <button className="btn node-actions-toggle-btn" onClick={this.toggleNodeActionsVisibility}/>
        <div className="sidebar-node-actions">
          <button className="btn node-actions-btn node-actions-edit-btn" onClick={this.handleEditClick(host)}
                  disabled>
            <i className="fa fa-pencil-square-o" aria-hidden="true"/>
          </button>
          <button className="btn node-actions-btn node-actions-remove-btn" onClick={this.handleDeleteClick(host)}>
            <i className="fa fa-trash-o" aria-hidden="true"/>
          </button>
        </div>
      </li>
    )
  }
}

export default connect(null, {showModal})(SidebarSubMenuItem)
