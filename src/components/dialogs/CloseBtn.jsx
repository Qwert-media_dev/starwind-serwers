import React from 'react'

export default ({onClick, small}) => (
  <button className={'dialog-close-btn' + (small ? ' dialog-close-btn--small' : '')} onClick={onClick}>
    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 21.18 21.188" fill="#4a4a4a" fillRule="evenodd">
      <path d="M19.78-.013L21.2 1.4 1.4 21.2l-1.42-1.413zm-18.38 0l19.8 19.8-1.42 1.414L-.02 1.4z"/>
    </svg>
  </button>
)
