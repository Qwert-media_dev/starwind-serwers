import Dialog from 'material-ui/Dialog'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { connectModal } from 'redux-modal'
import { unbindNode } from '../../actions/index'
import { Modal } from '../../constants'
import CloseBtn from './CloseBtn'
import './NodeDeleteDialog.css'

class NodeDeleteDialog extends Component {
  static propTypes = {
    // from connectModal
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleDestroy: PropTypes.func.isRequired,
    node: PropTypes.object.isRequired,
    // from connect
    nodes: PropTypes.array.isRequired,
    unbindNode: PropTypes.func.isRequired,
    // from router
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
  }

  state = {
    redirectTo: null,
    requestInProgress: false
  }

  componentWillReceiveProps() {
    if (this.state.redirectTo) this.setState({redirectTo: null})
  }

  handleDeleteClick = () => {
    const {unbindNode, node, handleHide, match, nodes} = this.props
    const nodeIndex = nodes.indexOf(node)
    const nodeSibling = nodes[nodeIndex - 1] || nodes[nodeIndex + 1]
    const redirectTo = match.params.uid === node.uid && nodeSibling ? `/nodes/${nodeSibling.uid}` : null

    this.setState({requestInProgress: true})

    unbindNode(node).then(cb => {
      this.afterHide = () => this.setState({
        requestInProgress: false,
        redirectTo
      }, cb)
      handleHide()
    })
  }

  onExited = () => {
    if (this.afterHide) this.afterHide()
    this.props.handleDestroy()
  }

  render() {
    const {show, handleHide, node} = this.props
    const {redirectTo, requestInProgress} = this.state

    if (redirectTo) return <Redirect to={redirectTo}/>

    return (
      <Dialog open={show} onRequestClose={handleHide} onExited={this.onExited}>
        <div className="dialog">
          <CloseBtn onClick={handleHide}/>
          <h4 className="dialog-title" style={{borderBottom: '1px solid #dbdbdb'}}>Delete This Node?</h4>
          <div className="dialog-content text-center">
            <div className="delete-node-dialog-data">
              <p className="delete-node-dialog-label">Node Name</p>
              <p className="delete-node-dialog-value">{node.ipAddress}</p>
              {/*<p className="delete-node-dialog-label">Node Host</p>*/}
              {/*<p className="delete-node-dialog-value"></p>*/}
            </div>
            <div className="delete-node-dialog-actions">
              <button className="btn btn-secondary" style={{marginRight: 12}} onClick={handleHide}>Cancel</button>
              <button className="btn btn-primary" onClick={this.handleDeleteClick} disabled={requestInProgress}>Delete
              </button>
            </div>
          </div>
        </div>
      </Dialog>
    )
  }
}

export default connectModal({
  name: Modal.NODE_DELETE,
  destroyOnHide: false
})(connect(state => ({
  nodes: state.nodes
}), {unbindNode})(NodeDeleteDialog))
