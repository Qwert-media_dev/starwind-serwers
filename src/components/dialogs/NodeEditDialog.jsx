import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connectModal } from 'redux-modal'
import { Modal } from '../../constants'
import CloseBtn from './CloseBtn'

class NodeEditDialog extends Component {
  static propTypes = {
    // form connect modal
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleDestroy: PropTypes.func.isRequired,
    node: PropTypes.object
  }

  state = {
    nodeName: '',
    nodeHost: ''
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.node)
      this.setState({nodeName: nextProps.node.ipAddress})
  }

  handleSubmit = e => {
    e.preventDefault()
    // TODO: edit node
    this.props.handleHide()
  }

  onExited = () => {
    this.props.handleDestroy()
  }

  render() {
    const {nodeName, nodeHost} = this.state
    const {show, handleHide} = this.props
    const disabled = !nodeName || !nodeHost

    return (
      <Dialog open={show} onRequestClose={handleHide} onExited={this.onExited}>
        <div className="dialog">
          <CloseBtn onClick={handleHide}/>
          <h4 className='dialog-title'>Edit Node</h4>
          <div className='dialog-content'>
            <form onSubmit={this.handleSubmit}>
              <TextField
                label='Node Name'
                value={nodeName}
                onChange={e => this.setState({nodeName: e.target.value})}
                style={{marginBottom: 17}}
                fullWidth
              />
              <TextField
                label='Node Host'
                value={nodeHost}
                onChange={e => this.setState({nodeHost: e.target.value})}
                style={{marginBottom: 40}}
                fullWidth
              />
              <button className='btn btn-primary align-center' disabled={disabled}>Save</button>
            </form>
          </div>
        </div>
      </Dialog>
    )
  }
}

export default connectModal({
  name: Modal.NODE_EDIT,
  destroyOnHide: false
})(NodeEditDialog)
