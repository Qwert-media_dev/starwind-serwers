import TextField from 'material-ui/TextField'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { connectModal } from 'redux-modal'
import { bindNode } from '../../actions/index'
import { Modal } from '../../constants'
import CloseBtn from './CloseBtn'
import './NodeCreateDialog.css'

class NodeCreateDialog extends Component {
  static propTypes = {
    // from connect modal
    show: PropTypes.bool.isRequired,
    handleHide: PropTypes.func.isRequired,
    // form connect
    bindNode: PropTypes.func.isRequired,
  }

  state = {
    nodeName: '',
    nodeIp: '',
    requestInProgress: false,
    redirectTo: null
  }

  componentDidMount() {
    document.addEventListener('click', this.handleDocuemntClick)
    document.addEventListener('keydown', this.handleDocumentKeydown)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocuemntClick)
    document.removeEventListener('keydown', this.handleDocumentKeydown)

  }

  handleDocuemntClick = e => {
    if (!e.target.closest('.create-node-dialog')) this.props.handleHide()
  }

  handleDocumentKeydown = e => {
    if (e.keyCode === 27) this.props.handleHide()
  }

  handleSubmit = e => {
    e.preventDefault()
    const {nodeName, nodeIp} = this.state
    const {bindNode, handleHide} = this.props

    this.setState({requestInProgress: true})

    bindNode(nodeName, nodeIp).then(({payload: nodes}) => {
      const newNode = nodes.find(node => node.ipAddress === this.state.nodeName) // TODO: change ipAddress to name

      this.setState({
        nodeName: '',
        nodeIp: '',
        requestInProgress: false,
        redirectTo: `/nodes/${newNode.uid}`
      }, handleHide)
    })
  }

  render() {
    const {nodeName, nodeIp, requestInProgress, redirectTo} = this.state
    const {handleHide} = this.props
    const disabled = !nodeName || !nodeIp || requestInProgress

    if (redirectTo)
      return <Redirect to={redirectTo}/>

    return (
      <div className="create-node-dialog">
        <CloseBtn onClick={handleHide} small/>
        <h4 className="create-node-dialog-title">New Node</h4>
        <form onSubmit={this.handleSubmit}>
          <TextField
            label="Node Name"
            value={nodeName}
            onChange={e => this.setState({nodeName: e.target.value})}
            fullWidth
            style={{marginBottom: 17}}
          />
          <TextField
            label="Node Ip"
            value={nodeIp}
            onChange={e => this.setState({nodeIp: e.target.value})}
            fullWidth
            style={{marginBottom: 17}}
          />
          <button className="btn btn-primary btn-small align-center" disabled={disabled}>Add Node</button>
        </form>
      </div>
    )
  }
}

export default connectModal({
  name: Modal.NODE_CREATE
})(connect(null, {bindNode})(NodeCreateDialog))
