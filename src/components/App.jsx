import React from 'react'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import { Modal } from '../constants'
import './App.css'
import Content from './content/Content'
import NodeDeleteDialog from './dialogs/NodeDeleteDialog'
import NodeEditDialog from './dialogs/NodeEditDialog'
import Header from './Header'
import NodeRoute from './navigation/NodeRoute'
import MainSidebar from './sidebar/MainSidebar'
import WizardForm from './web-forms/WIzardForm'


const App = ({location, wizardFormIsVisible}) => (
  <div className="app-page">
    <MainSidebar location={location}/>
    <main className="app-main"
          style={wizardFormIsVisible ? {overflowY: 'hidden', height: '100vh', paddingRight: 17} : {}}>
      <Header/>
      <div className="app-content">
        <NodeRoute path='/nodes/:uid/(views)?/:view?' component={Content} location={location}/>
      </div>
      <WizardForm/>
    </main>
    <NodeEditDialog/>
    <Route path='/(nodes)?/:uid?' component={NodeDeleteDialog}/>
  </div>
)

export default connect(state => ({
  wizardFormIsVisible: !!state.modal[Modal.WIZARD_FORM]
}))(App)
