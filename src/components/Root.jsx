import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { applyMiddleware, compose, createStore } from 'redux'
import { Notifs } from 'redux-notifications'
import thunk from 'redux-thunk'
import rootReducer from '../reducers/index'
import App from './App'
import Login from './Login'
import PrivateRoute from './navigation/PrivateRoute'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))


const Root = () => (
  <Provider store={store}>
    <div>
      <Router basename={process.env.PUBLIC_URL}>
        <div>
          <Switch>
            <Route path='/login' component={Login}/>
            <PrivateRoute path='/' component={App}/>
          </Switch>
        </div>
      </Router>
      <Notifs componentClassName="app-notif"/>
    </div>
  </Provider>
)

export default Root
