import { actions as notifActions } from 'redux-notifications'
import { DISMISS_AFTER, Message } from '../constants'
import {
  bindNode as apiBindNode,
  getNodes as apiGetNodes,
  unbindNode as apiUnbindNode
} from '../dataProviders/starwind'
import { DELETE_NODE, SET_NODES } from '../reducers'

const {notifSend} = notifActions

export const getNodes = () => dispatch => apiGetNodes().then(
  payload => dispatch({type: SET_NODES, payload}),
  err => console.error(err) // eslint-disable-line
)

export const bindNode = (nodeName, nodeIp) => dispatch => apiBindNode(nodeName, nodeIp).then(
  () => {
    dispatch(notifSend({
      message: Message.NODE_ADDED,
      dismissAfter: DISMISS_AFTER
    }))
    return dispatch(getNodes())
  },
  err => console.error(err) // eslint-disable-line
)

export const unbindNode = node => dispatch => apiUnbindNode(node.uid).then(
  () => {
    dispatch({type: DELETE_NODE, payload: node})
    return () => dispatch(notifSend({
      message: Message.NODE_DELETE,
      dismissAfter: DISMISS_AFTER
    }))
  },
  err => console.error(err) // eslint-disable-line
)
