import { hide as hideModal, show as showModal } from 'redux-modal'
import { actions as notifActions } from 'redux-notifications'
import getInitialValues from '../components/web-forms/helpers/getInitialValues'
import getValidate from '../components/web-forms/helpers/getValidate'
import { DISMISS_AFTER, Modal } from '../constants'
import { getProperty, sendForm } from '../dataProviders'

const {notifSend} = notifActions


export const showForm = (pluginUid, dataSource, nodeUid) => dispatch =>
  getProperty(pluginUid, dataSource, nodeUid).then(
    payload => dispatch(showModal(Modal.WIZARD_FORM, {
      data: payload,
      initialValues: getInitialValues(payload),
      validate: getValidate(payload),
      asyncValidate: getAsyncValidate(payload),
      onSubmit: onSubmit(payload),
      onSubmitFail
    })),
    err => console.error(err) // eslint-disable-line
  )

export const hideForm = () => dispatch => dispatch(hideModal(Modal.WIZARD_FORM))

const onSubmit = data => (values, dispatch) => {
  return sendForm(data.submitUrl, values).then(
    ({message}) => {
      console.log(`submitting to ${data.submitUrl}`, values) // eslint-disable-line
      dispatch(hideForm())
      dispatch(notifSend({
        message,
        dismissAfter: DISMISS_AFTER
      }))
    },
    err => console.error(err) // eslint-disable-line
  )
}

const onSubmitFail = errors => {
  if (Object.keys(errors).length > 0) {
    document.querySelector('.form-render__step_invalid').scrollIntoView()
  }
}

const getAsyncValidate = data => values => {
  return sendForm(data.validateUrl, values)
}
