import Cookie from 'js-cookie'
import { login } from '../dataProviders'
import { AUTHENTICATE, SIGN_OUT } from '../reducers'

export const authenticate = (userLogin, password, remember) => dispatch =>
  login(userLogin, password).then(() => {
    const token = Math.random().toString().split('.').pop()
    Cookie.set('token', token, {expires: remember ? 1000 : null})
    dispatch({type: AUTHENTICATE})
  })

export function signOut() {
  Cookie.remove('token')
  return {type: SIGN_OUT}
}
